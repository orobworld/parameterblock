package parameters;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.OptionalDouble;
import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.JTextComponent;

/**
 * ParameterBlock is the parent class for repositories of parameter settings.
 *
 * Subclasses will typically need only to provide a constructor that defines
 * all the parameters and sets their current and default values, their names
 * and their tool tips.
 *
 * All instances of subclasses should be created using the newBlock() factory
 * method.
 *
 * Parameters should be defined in the order in which they are to appear in
 * user dialogs.
 *
 * Subclasses must implement the values() method to provide a list of values
 * for each enumerated parameter.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public abstract class ParameterBlock implements Serializable {
  private static final long serialVersionUID = 1L;
  // Column indices of a parameter matrix.
  /** PNAME indexes the parameter name column. */
  public static final int PNAME = 0;
  /** PTYPE indexes the parameter type column. */
  public static final int PTYPE = 1;
  /** PVALUE indexes the parameter value column. */
  public static  final int PVALUE = 2;
  /** PTIP indexes the parameter tool tip column. */
  public static final int PTIP = 3;
  /** PCOLCOUNT gives the number of columns in a parameter matrix. */
  public static final int PCOLCOUNT = 4;

  // The extending class must initialize these arrays, all indexed by
  // parameter, in its constructor.
  /** The value array holds the current values of parameters. */
  protected Object[] value;
  /** The defaultValue array holds the default values of parameters. */
  protected Object[] defaultValue;
  /** The name array hold strings used to identify parameters. */
  protected String[] name;
  /** The tip array holds tool tips used by input dialogs. */
  protected String[] tip;
  /** The type array holds the type/class of each parameter. */
  protected Class[] type;
  /** The nParameters field holds the block's parameters count. */
  protected final int nParameters;
  /** The editable array indicates which fields can be altered by the user.
   See the documentation of the editable() method for details.*/
  protected final boolean[] editable;

  /**
   * Constructor.
   * @param n the number of parameters
   */
  protected ParameterBlock(final int n) {
    nParameters = n;
    editable = new boolean[n];
    for (int i = 0; i < n; i++) {
      editable[i] = true;
    }
    // Declare the arrays (except current value).
    defaultValue = new Object[nParameters];
    type = new Class[nParameters];
    name = new String[nParameters];
    tip = new String[nParameters];
  }

  /**
   * Creates an instance of a parameter block.
   * @param <T> the type of block to create
   * @param cls the Class of the target block type
   * @return a new parameter block of the chosen type
   * @throws InstantiationException if an instance cannot be created
   * @throws IllegalAccessException if a field cannot be created (?)
   */
  public static final <T extends ParameterBlock> T newBlock(final Class<T> cls)
         throws InstantiationException, IllegalAccessException {
    T block;
    try {
      block = cls.getDeclaredConstructor().newInstance();
    } catch (InvocationTargetException | NoSuchMethodException ex) {
      throw new InstantiationException(ex.getMessage());
    }
    // Initialize parameters to default values.
    block.init();
    return block;
  }

  /**
   * Resets all parameters to default values.
   */
  public final void init() {
    value = Arrays.copyOf(defaultValue, defaultValue.length);
  }

  /**
   * Gets the current value of a parameter.
   * @param param the parameter to fetch
   * @return the current value
   */
  public final Object getValue(final int param) {
    return value[param];
  }

  /**
   * Sets the value of a parameter.
   * @param param the parameter to set
   * @param val the new value
   */
  public final void setValue(final int param, final Object val) {
    if (value[param] != val) {
      value[param] = val;
    }
  }

  /**
   * Gets the default value of a parameter.
   * @param param the parameter
   * @return the default value
   */
  public final Object getDefault(final int param) {
    return defaultValue[param];
  }

  /**
   * Gets the name of a parameter.
   * @param param the parameter
   * @return the parameter's string representation
   */
  public final String getParameterName(final int param) {
    return name[param];
  }

  /**
   * Gets the tool tip string for a parameter.
   * @param param the parameter
   * @return the tool tip for this parameter
   */
  public final String getTip(final int param) {
    return tip[param];
  }

  /**
   * Gets the type of a parameter.
   * @param param the parameter
   * @return the parameter's class
   */
  public final Class getType(final int param) {
    return type[param];
  }

  /**
   * Gets a table of all parameters. Column 0 contains the parameters' text
   * names, column 1 their classes, column 2 their current values, and
   * column 3 tool tips for input dialogs.
   * @return a table of all parameters (name, type, value)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public final Object[][] getAll() {
    Object[][] all = new Object[nParameters][4];
    for (int i = 0; i < nParameters; i++) {
      all[i][0] = name[i];
      all[i][1] = type[i];
      all[i][2] = value[i];
      all[i][3] = tip[i];
    }
    return all;
  }

  /**
   * Updates the values of the parameters.
   * @param all a vector of parameter values, indexed by parameter
   */
  public final void update(final Object[] all) {
    if (all.length == name.length) {
      for (int i = 0; i < all.length; i++) {
        // Look for changes, skipping any secondary parameter blocks.
        if (!(type[i].isAssignableFrom(ParameterBlock.class))
            && (value[i] != all[i])) {
          value[i] = all[i];
        }
      }
    } else {
      // Something happeneed that should never happen.
      throw new RuntimeException("Number of values supplied ("
                                 + all.length
                                 + ") does not match number of "
                                 + "parameters ("
                                 + name.length + ").");
    }
  }

  /**
   * Splits a parameter (string) containing multiple 'Name=Value' pairs
   * into a matrix where the first column is the parameter name and the second
   * is the value.
   * @param param the parameter split
   * @return a matrix of parameter names and values
   */
  public final String[][] split(final int param) {
    String[] intermediate = ((String) value[param]).split("[,;: \t]+");
    String[][] params = new String[intermediate.length][];
    for (int i = 0; i < intermediate.length; i++) {
      params[i] = intermediate[i].split("=");
    }
    return params;
  }

  /**
   * Obtains a vector of all possible values of an enumerated parameter.
   * @param param the parameter
   * @return a vector of values (null if the argument is not enumerated)
   */
  public abstract Object[] values(final int param);

  /**
   * Determines which parameters can be edited/changed.
   *
   * This method should be called by the GUI whenever a parameter changes value
   * in a dialog. Its purpose is to allow some parameters to be contingent
   * on the value of others (meaning that certain combinations of parameter
   * values may make a particular parameter irrelevant, or force a particular
   * value for it).
   *
   * The default behavior is to make all parameters editable. Subclasses should
   * override the setEditable method if they need contingent control over
   * parameters.
   *
   * @param values the current parameter values in the dialog
   * @return a vector of boolean values (true if the corresponding parameter
   * can be edited)
   */
  public final boolean[] areEditable(final Object[] values) {
    boolean[] ed = Arrays.copyOf(editable, nParameters);
    setEditable(ed, values);
    return ed;
  }

  /**
   * Sets flags indicating whether each parameter is editable by the user.
   * On input, the 'editable' argument is all true. The implementing class
   * should override this method and set editable[j] false if the current
   * values contraindicate editing parameter j. If the subclass has no
   * contingent parameters, it should not override the default method.
   * @param ed a vector of flags (true if editable), all true on input
   * @param values the current parameter values in the dialog
   */
  protected void setEditable(final boolean[] ed, final Object[] values) { }

  /**
   * Generates a summary of the current parameter settings.
   * @return a string summarizing the current settings
   */
  @Override
  public final String toString() {
    StringBuilder sb = new StringBuilder();
    // Append each parameter's name and value.
    for (int i = 0; i < nParameters; i++) {
      sb.append("\t").append(name[i]).append(" = ")
        .append(value[i]).append("\n");
    }
    return sb.toString();
  }

  /**
   * Displays a dialog showing current values in a parameter block.
   * If the user makes and accepts changes, the function parses the new values
   * and returns them. If no changes were made, the function returns null.
   *
   * The parameter array has four columns:
   *   name (column 0);
   *   class (1);
   *   value (2);
   *   tool tip (3).
   *
   * @param parent the parent window for the dialog
   * @param dTitle the title for the dialog
   * @param block the parameter block
   * @return the new parameter values (null if the user cancels)
   */
  @SuppressWarnings({ "checkstyle:magicnumber", "checkstyle:methodlength" })
  public static final Object[] showDialog(final JFrame parent,
                                          final String dTitle,
                                          final ParameterBlock block) {
    // Get all the parameters in the block.
    Object[][] params = block.getAll();
    // Set up a mapping of radio button models to enumerated parameter values.
    final HashMap<ButtonModel, Object> buttonMap = new HashMap<>();
    /*
      Store an input component for each parameter. For a boolean parameter,
      store a check box; for an enumerated parameter, store a radio button
      group; for any other component, store a text field.
    */
    final Object[] component = new Object[params.length];
    // Copy parameters so they can be accessed in the OK button action method.
    final Object[][] p = new Object[params.length][];
    for (int i = 0; i < params.length; i++) {
      p[i] = Arrays.copyOf(params[i], PCOLCOUNT);
    }
    // Allocate a return array.
    final Object[] values = new Object[p.length];
    // Create a semaphore to indicate whether the user chose OK or not.
    final boolean[] go = new boolean[] {false};
    // Create a panel to contain the parameters.
    final JPanel panel = new JPanel(new GridBagLayout());
    GridBagConstraints c;
    // Create a change listener to update which components are enabled
    // when an input changes.
    final ChangeListener reenable = (ChangeEvent e) -> {
      // Get the current parameter settings in the dialog.
      getParameterChoices(parent, component, buttonMap, p, values);
      // Query the parameter block as to which parameters remain editable.
      boolean[] ed = block.areEditable(values);
      // Update control states accordingly.
      for (int i = 0; i < component.length; i++) {
        if (component[i] instanceof ButtonGroup) {
          ButtonGroup bg = (ButtonGroup) component[i];
          Enumeration<AbstractButton> buttons = bg.getElements();
          while (buttons.hasMoreElements()) {
            buttons.nextElement().setEnabled(ed[i]);
          }
        } else {
          ((Component) component[i]).setEnabled(ed[i]);
        }
      }
    };
    // Populate the panel with the parameter names and current values.
    for (int i = 0; i < params.length; i++) {
      JLabel name = new JLabel((String) params[i][PNAME]);
      name.setToolTipText((String) params[i][PTIP]);
      c = new GridBagConstraints(0, i + 1, 1, 1, 0, 0,
                                 GridBagConstraints.WEST,
                                 GridBagConstraints.HORIZONTAL,
                                 new Insets(4, 4, 4, 4),
                                 0, 0);
      panel.add(name, c);
      JComponent input;
      if (params[i][PTYPE] == boolean.class
          || params[i][PTYPE] == Boolean.class) {
        JCheckBox box = new JCheckBox();
        box.setSelected((boolean) params[i][PVALUE]);
        input = box;
        component[i] = input;
        box.addChangeListener(reenable);
      } else if (params[i][PVALUE] instanceof Enum) {
        ButtonGroup bg = new ButtonGroup();
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
        input = new JScrollPane(buttonPanel);
        for (Object v : block.values(i)) {
          JRadioButton button = new JRadioButton(v.toString());
          bg.add(button);
          button.setSelected(v == params[i][PVALUE]);
          buttonPanel.add(button);
          buttonMap.put(button.getModel(), v);
          button.addChangeListener(reenable);
        }
        component[i] = bg;
      } else {
        // Use a text field.
        String v = "";
        if (params[i][PTYPE] == OptionalDouble.class) {
          if (((OptionalDouble) params[i][PVALUE]).isPresent()) {
            v = Double.toString(((OptionalDouble) params[i][PVALUE])
                                 .getAsDouble());
          } else {
            v = null;
          }
        } else {
          v = params[i][PVALUE].toString();
        }
        JTextField text = new JTextField(v);
        input = text;
        component[i] = input;
        // Note: Text fields cannot control whether parameters are enabled,
        // so no change listener is required here.
      }
      input.setToolTipText((String) params[i][PTIP]);
      c = new GridBagConstraints(1, i + 1, 1, 1, 1.0, 1.0,
                                 GridBagConstraints.CENTER,
                                 GridBagConstraints.HORIZONTAL,
                                 new Insets(4, 4, 4, 4),
                                 0, 0);
      panel.add(input, c);
    }
    JScrollPane sp = new JScrollPane(panel);
    // Create a modal dialog.
    final JDialog d = new JDialog(parent, dTitle, false);
    d.setLocationByPlatform(true);
    // Center the dialog on the screen.
    d.setLocationRelativeTo(null);
    d.setModal(true);
    d.setLayout(new GridBagLayout());
    // Add the scroll pane spanning two columns.
    c = new GridBagConstraints(0, 0, 3, 1, 1.0, 1.0,
                               GridBagConstraints.CENTER,
                               GridBagConstraints.BOTH,
                               new Insets(4, 4, 4, 4), 0, 0);
    d.add(sp, c);
    // Add an OK button (second row, left column).
    JButton ok = new JButton("OK");
    ok.addActionListener((ActionEvent e) -> {
      getParameterChoices(parent, component, buttonMap, p, values);
      // Signal that OK was chosen.
      go[0] = true;
      // Close the window.
      d.dispose();
    });
    c = new GridBagConstraints(0, 1, 1, 1, 0.5, 0,
                               GridBagConstraints.WEST,
                               GridBagConstraints.NONE,
                               new Insets(4, 4, 4, 4), 0, 0);
    d.add(ok, c);
    // Add a Reset button (second row, middle column).
    JButton reset = new JButton("Reset");
    reset.setToolTipText("Reset to default values");
    reset.addActionListener((ActionEvent e) -> {
      // Reset the parameter values in the dialog controls, based on a new
      // block with default values.
      try {
        Object[][] p0 = ParameterBlock.newBlock(block.getClass()).getAll();
        initParamControls(component, p0);
      } catch (IllegalAccessException | InstantiationException ex) {
        // If creating a new block with default values fails for some reason,
        // alert the user.
        JOptionPane.showMessageDialog(
          parent,
          "Cannot Reset Parameters",
          "Unable to create a new parameter block with default values!"
            + System.lineSeparator() + ex.getMessage(),
           JOptionPane.ERROR_MESSAGE);
      }
    });
    c = new GridBagConstraints(1, 1, 1, 1, 0.5, 0,
                               GridBagConstraints.CENTER,
                               GridBagConstraints.NONE,
                               new Insets(4, 4, 4, 4), 0, 0);
    d.add(reset, c);
    // Add a Cancel button (second row, right column).
    JButton cancel = new JButton("Cancel");
    cancel.addActionListener((ActionEvent e) -> {
      // Close the window.
      d.dispose();
    });
    c = new GridBagConstraints(2, 1, 1, 1, 0.5, 0,
                               GridBagConstraints.EAST,
                               GridBagConstraints.NONE,
                               new Insets(4, 4, 4, 4), 0, 0);
    d.add(cancel, c);
    // Display the dialog and let the user modify parameters if desired.
    d.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    d.setSize(new Dimension(450, 300));
    d.setVisible(true);
    // After the dialog has closed, return the results (if any).
    if (go[0]) {
      return values;
    } else {
      return null;
    }
  }

  /**
   * Recovers the current parameter values from a parameter block dialog.
   * Note: Because the result is used in a lambda, it has to be passed in
   * as a parameter, not as a return value.
   * @param parent the window (JFrame) that will be the parent for any dialogs
   * @param component the vector of input components in the dialog
   * @param buttonMap a map of dialog button models to owning components
   * @param p the matrix of parameters
   * @param values a vector in which the current parameter values from the
   * dialog are returned
   */
  private static void getParameterChoices(
    final JFrame parent,
    final Object[] component,
    final Map<ButtonModel, Object> buttonMap,
    final Object[][] p,
    final Object[] values) {
    // Parse all the input fields into the correct classes and return them.
    for (int i = 0; i < p.length; i++) {
      if (p[i][1] == boolean.class || p[i][1] == Boolean.class) {
        // Read the value of a check box.
        values[i] = ((AbstractButton) component[i]).isSelected();
      } else if (p[i][2] instanceof Enum) {
        // Enumerated parameter: use the selected radio button to get
        // the chosen value.
        ButtonModel m = ((ButtonGroup) component[i]).getSelection();
        values[i] = buttonMap.get(m);
      } else {
        // Read and parse the value of a text field.
        String v = ((JTextComponent) component[i]).getText();
        try {
          if (p[i][1] == int.class
                  || p[i][1] == Integer.class) {
            values[i] = Integer.parseInt(v);
          } else if (p[i][1] == long.class
                  || p[i][1] == Long.class) {
            values[i] = Long.parseLong(v);
          } else if (p[i][1] == double.class
                  || p[i][1] == Double.class) {
            values[i] = Double.parseDouble(v);
          } else if (p[i][1] == OptionalDouble.class) {
            if (v == null || v.isEmpty()) {
              values[i] = OptionalDouble.empty();
            } else {
              values[i] = OptionalDouble.of(Double.parseDouble(v));
            }
          } else {
            // Assume it's a string.
            values[i] = v;
          }
        } catch (NumberFormatException ex) {
          // If a field is indigestible, alert the user and then return null.
          JOptionPane.showMessageDialog(
                  parent.getParent(),
                  "The entry '" + v + "' for parameter "
                          + p[i][0].toString()
                          + " is not a valid "
                          + p[i][1].toString() + ".",
                  "Invalid Input",
                  JOptionPane.ERROR_MESSAGE);
        }
      }
    }
  }


  /**
   * Resets the values in a parameter dialog.
   * @param controls the vector of input controls in the dialog
   * @param params the matrix of parameter data
   */
  private static void initParamControls(final Object[] controls,
                                        final Object[][] params) {
    for (int i = 0; i < controls.length; i++) {
      Object c = controls[i];
      if (c instanceof JCheckBox) {
        ((AbstractButton) c).setSelected((boolean) params[i][PVALUE]);
      } else if (c instanceof ButtonGroup) {
        ButtonGroup bg = (ButtonGroup) c;
        String v = params[i][PVALUE].toString();
        Enumeration<AbstractButton> elts = bg.getElements();
        while (elts.hasMoreElements()) {
          AbstractButton b = elts.nextElement();
          if (b.getText().equals(v)) {
            b.setSelected(true);
            break;
          }
        }
      } else if (c instanceof JTextField) {
        String v;
        if (params[i][PTYPE] == OptionalDouble.class) {
          if (((OptionalDouble) params[i][PVALUE]).isPresent()) {
            v = Double.toString(((OptionalDouble) params[i][PVALUE])
                                 .getAsDouble());
          } else {
            v = null;
          }
        } else {
          v = params[i][PVALUE].toString();
        }
        ((JTextComponent) c).setText(v);
      }
    }
  }
}
