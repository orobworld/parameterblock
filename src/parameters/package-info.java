/**
 * A class to facilitate storage of and access to parameters used by algorithms.
 *
 * {@link ParameterBlock} is an abstract class that provides the basic structure
 * for storing and retrieving parameters. It can handle parameters of any
 * type/class.
 *
 * To utilize it, do the following:
 * <ul><li>For each collection of parameters in the application, create a class
 * that extends {@link ParameterBlock}.</li>
 * <li>In that class, declare public static integer fields indexing each
 * distinct parameter (so that you can access parameters via symbolic
 * names).</li>
 * <li>Give the class a public constructor.
 *   <ul><li>Begin the constructor by calling <code>super()</code> with the
 * number of parameters in the class.</li>
 * <li>Fill the arrays <code>name[]</code>, <code>type[]</code>,
 * <code>defaultValue[]</code> and <code>tip</code> (declared in the parent
 * class) with each parameter's string label, class, default value and tool
 * tip, respectively.</li>
 * <li>Implement the <code>values()</code> method to return the values of any
 * enumerated parameters.</li></ul>
 * <li>Within the program, create an instance of the subclass using the static
 * <code>ParameterBlock.newBlock()</code> factory method.</li>
 * <li>If the ability to edit some parameters is contingent on other factors
 * (possibly but not necessarily other parameter choices), use the
 * <code>setEditable()</code> method to control whether they are editable and
 * the <code>areEditable()</code> method to tell whether they are currently
 * editable.</li>
 * <li>Use the <code>setValue()</code> and <code>getValue()</code> methods to
 * set or get individual parameter values.</li>
 * <li>In a Swing application, use the static
 * <code>ParameterBlock.showDialog()</code> method to display a dialog window
 * allowing the user to see and modify parameter settings. If changes are
 * made by the user, use the <code>update()</code> method to change the values
 * in the parameter block.</li></ul>
 * <p>
 * <p>{@link ParameterBlock} also provides a utility method <code>split()</code>
 * that takes as its argument the index of a parameter in a block. It assumes
 * the parameter is a string consisting of multiple "key=value" pairs
 * (separated by any combinations of commas, semicolons, colons, spaces or
 * tabs), splits those pairs, and returns a matrix with keys in the first
 * column and values in the second.
 */
package parameters;
