package demo;

/**
 * MIPEmphasis enumerates possible emphasis choices for a MIP solver.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public enum MIPEmphasis {
  /** OPTIMALITY emphasizes proving optimality. */
  OPTIMALITY("Proving optimality"),
  /** FEASIBILITY emphasizes finding feasible solutions. */
  FEASIBILITY("Finding feasibile solutions"),
  /** BESTBOUND emphasizes improving the best bound. */
  BESTBOUND("Improving the best bound");

  private final String name;

  /**
   * Constructor.
   * @param id a string ID for the value
   */
  MIPEmphasis(final String id) {
    name = id;
  }

  /**
   * Expresses the value as a string.
   * @return a string version of the choice
   */
  @Override
  public String toString() {
    return name;
  }
}
