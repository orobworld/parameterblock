package demo;

import parameters.ParameterBlock;

/**
 * MIPBlock holds parameters specific to a MIP solver.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIPBlock extends ParameterBlock {
  /** EMPHASIS controls what goal the solver emphasizes. */
  public static final int EMPHASIS = 0;
  /** SYMMETRY chooses whether to add antisymmetry constraints. */
  public static final int SYMMETRY = 1;
  /** NODELIMIT limits the number of nodes visited in the search tree. */
  public static final int NODELIMIT = 2;
  /** OTHER consists of key=value pairs in string form. */
  public static final int OTHER = 3;
  private static final int PARAMETERCOUNT = 4;

  /**
   * Constructor.
   */
  public MIPBlock() {
    // Call the parent constructor.
    super(PARAMETERCOUNT);
    // Initialize the arrays (except current value).
    name[EMPHASIS] = "MIP Emphasis";
    type[EMPHASIS] = MIPEmphasis.class;
    defaultValue[EMPHASIS] = MIPEmphasis.OPTIMALITY;
    tip[EMPHASIS] = "What goal the solver should emphasize";
    name[SYMMETRY] = "Antisymmetry";
    type[SYMMETRY] = boolean.class;
    defaultValue[SYMMETRY] = false;
    tip[SYMMETRY] = "Should the solver add constraints to reduce symmetry?";
    name[NODELIMIT] = "Node limit";
    type[NODELIMIT] = Integer.class;
    defaultValue[NODELIMIT] = Integer.MAX_VALUE;
    tip[NODELIMIT] = "The maximum number of nodes to visit in the search tree";
    name[OTHER] = "Other parameters";
    type[OTHER] = String.class;
    defaultValue[OTHER] = "NodeSelect=1;LogInterval=3;";
    tip[OTHER] = "Key=Value pairs";
  }

  /**
   * Obtains a vector of all possible values of an enumerated parameter.
   * The only enumerated parameter here is the MIP emphasis.
   * @param param the parameter
   * @return a vector of values (null if the argument is not enumerated)
   */
  @Override
  public Object[] values(final int param) {
    if (param == EMPHASIS) {
      return MIPEmphasis.values();
    } else {
      throw new IllegalArgumentException("Not an enumerated parameter.");
    }
  }

}
