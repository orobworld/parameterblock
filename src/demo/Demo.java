package demo;

/**
 * Demo runs the ParameterBlock demonstration.
 *
 * It opens a window (XFrame) for the demonstration but attaches a controller,
 * but otherwise does nothing.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Demo {

  /**
   * Null constructor.
   */
  private Demo() { }

  /**
   * Runs the demonstration.
   * @param args the command line arguments (ignored)
   */
  public static void main(final String[] args) {
    // Create a window (with no controller) and exit.
    XFrame frame = new XFrame();
    DemoController controller = new DemoController(frame);
    frame.setController(controller);
    frame.setVisible(true);
  }

}
