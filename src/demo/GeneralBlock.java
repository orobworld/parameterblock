package demo;

import parameters.ParameterBlock;

/**
 * GeneralBlock holds parameters that are common to all solvers.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class GeneralBlock extends ParameterBlock {
  /** SOLVERTYPE selects the type of solver to use. */
  public static final int SOLVERTYPE = 0;
  /** TIMELIMIT is the time limit for the solver. */
  public static final int TIMELIMIT = 1;
  private static final int PARAMETERCOUNT = 2;

  /**
   * Constructor.
   */
  public GeneralBlock() {
    // Call the parent constructor.
    super(PARAMETERCOUNT);
    // Initialize the arrays (except current value).
    name[SOLVERTYPE] = "Solver type";
    type[SOLVERTYPE] = SolverType.class;
    defaultValue[SOLVERTYPE] = SolverType.MIPSolver;
    tip[SOLVERTYPE] = "The type of solver to use";
    name[TIMELIMIT] = "Time limit";
    type[TIMELIMIT] = double.class;
    defaultValue[TIMELIMIT] = 0;
    tip[TIMELIMIT] = "The time limit for the solver (in seconds)";
  }

  /**
   * Obtains a vector of all possible values of an enumerated parameter.
   * The only enumerated parameter here is the solver type.
   * @param param the parameter
   * @return a vector of values (null if the argument is not enumerated)
   */
  @Override
  public Object[] values(final int param) {
    if (param == SOLVERTYPE) {
      return SolverType.values();
    } else {
      throw new IllegalArgumentException("Not an enumerated parameter.");
    }
  }

}
