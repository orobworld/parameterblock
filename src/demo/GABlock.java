package demo;

import parameters.ParameterBlock;

/**
 * GABlock holds parameters for a genetic algorithm.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class GABlock extends ParameterBlock {
  /** POPULATION sets the population size. */
  public static final int POPULATION = 0;
  /** ELITE chooses whether to enforce elitism. */
  public static final int ELITE = 1;
  /** MUTATE sets the mutation rate. */
  public static final int MUTATE = 2;
  private static final int PARAMETERCOUNT = 3;

  /**
   * Constructor.
   */
  public GABlock() {
    super(PARAMETERCOUNT);
    // Initialize the arrays (except current value).
    name[POPULATION] = "Population size";
    type[POPULATION] = int.class;
    defaultValue[POPULATION] = 100;
    tip[POPULATION] = "The size for each generation";
    name[ELITE] = "Elitism";
    type[ELITE] = boolean.class;
    defaultValue[ELITE] = true;
    tip[ELITE] = "Use elitism?";
    name[MUTATE] = "Mutation rate";
    type[MUTATE] = double.class;
    defaultValue[MUTATE] = 0.01;
    tip[MUTATE] = "The probability any individual offspring mutates";
  }

  /**
   * Obtains a vector of all possible values of an enumerated parameter.
   * There are no enumerated parameters here.
   * @param param the parameter
   * @return a vector of values (null if the argument is not enumerated)
   */
  @Override
  public Object[] values(final int param) {
    throw new UnsupportedOperationException("No enumerated parameters.");
  }

}
