package demo;

/**
 * SolverType enumerates types of solver codes.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public enum SolverType {
  /** MIPSolver is a MIP solver (CPLEX, Gurobi, XPRESS, ...). */
  MIPSolver("MIP solver"),
  /** GA is a genetic algorithm. */
  GA("genetic algorithm");

  private final String id;

  /**
   * Constructor.
   * @param name the name to use in printing
   */
  SolverType(final String name) {
    id = name;
  }

  /**
   * Express the choice as a string.
   * @return the string representation
   */
  @Override
  public String toString() {
    return id;
  }
}
