package demo;

import java.io.File;
import parameters.ParameterBlock;

/**
 * DemoController is the controller for the ParameterBlock demonstration.
 *
 * It exists to create and handle sample parameter blocks, which
 * typically would belong to the program logic and not the GUI.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class DemoController implements XFrameController {
  private final XFrame frame;          // the demo window
  private GeneralBlock general;        // general parameters
  private MIPBlock mip;                // MIP-specific parameters
  private GABlock ga;                  // GA-specific parameters
  private File currentDirectory;       // for saving output

  /**
   * Constructor.
   * @param xf the demo window
   */
  public DemoController(final XFrame xf) {
    frame = xf;
    // Assign stdout and stderr to the frame's central text area.
    System.setOut(frame.getCustomStream());
    System.setErr(frame.getCustomStream());
    // Set the starting directory to the user's home directory.
    currentDirectory = new File(System.getProperty("user.home"));
    // Create the paramter blocks.
    try {
      general = ParameterBlock.newBlock(GeneralBlock.class);
      mip = ParameterBlock.newBlock(MIPBlock.class);
      ga = ParameterBlock.newBlock(GABlock.class);
    } catch (IllegalAccessException | InstantiationException ex) {
      System.err.println("Cannot instantiate parameter block:\n"
                         + ex.getMessage());
    }
    // Print an opening message.
    System.out.println("To see the use of parameter blocks, use the "
      + "'Parameters' menu to make changes to the various blocks.\n"
      + "Hover over any entry for a tool tip.\n");
  }

  /**
   * Returns the current directory (the starting point for any file chooser
   * dialog).
   * @return the current directory
   */
  @Override
  public File getCurrentDirectory() {
    return currentDirectory;
  }

  /**
   * Sets the current directory.
   * @param target the new current directory
   */
  @Override
  public void setCurrentDirectory(final File target) {
    currentDirectory = target;
  }

  /**
   * Signals the program to perform a computational task that will run in
   * the background (so as not to interfere with the user interface).
   * @return a string report on the results of the computation (to be displayed
   * in the GUI)
   * @throws Exception (any exception the computation task might throw)
   */
  @Override
  public String runComputations() throws Exception {
    throw new UnsupportedOperationException("Not utilized by the demo.");
  }

  /**
   * Instructs the controller to abort the background task.
   * @return true if the task was successfully aborted
   */
  @Override
  public boolean abortTask() {
    throw new UnsupportedOperationException("Not utilized by the demo.");
  }

  /**
   * Gets the general parameter block.
   * @return the general parameter block
   */
  public GeneralBlock getGeneral() {
    return general;
  }

  /**
   * Get the relevant solver-specific parameter block.
   * @return the solver-specific block for the currently chosen solver.
   */
  public ParameterBlock getSpecific() {
    switch ((SolverType) general.getValue(GeneralBlock.SOLVERTYPE)) {
      case GA:
        return ga;
      case MIPSolver:
        return mip;
      default:  // (should never happen)
        return null;
    }
  }

  /**
   * Displays new parameter values after a block was updated.
   * @param block the block that was updated.
   */
  public void update(final ParameterBlock block) {
    System.out.println("\nNew values for parameter block "
                       + block.getClass() + ":");
    System.out.println(block);
    // If this is the MIP block, show how the key=value parameter splits.
    if (block == mip) {
      String[][] kv = mip.split(MIPBlock.OTHER);
      System.out.println("Results of splitting the 'OTHER' parameter:\n");
      for (String[] kv1 : kv) {
        System.out.println("\t" + kv1[0] + " = " + kv1[1]);
      }
    }
  }

}
