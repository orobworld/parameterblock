/**
 * A small program to demonstrate the use of the ParameterBlock class.
 * {@link Demo} sets up the demonstration and exits.
 * <p>
 * {@link XFrame} is the GUI (Swing window) for the application. It uses an
 * instance of {@link CustomOutputStream} to pipe stdin and stderr into the
 * central text area of the window.
 * <p>
 * {@link DemoController} is the program logic. It implements the
 * {@link XFrameController} interface to facilitate communication between the
 * GUI and it.
 * <p>
 * {@link GeneralBlock}, {@link MIPBlock} and {@link GABlock} are instances
 * of {@link parameters.ParameterBlock}.
 * <p>
 * {@link MIPEmphasis} and {@link SolverType} are examples of enumeration types
 * used by parameters.
 */
package demo;
