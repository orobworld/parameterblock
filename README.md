# Parameter Blocks
The **ParameterBlock** class (in the `parameters` package) is an abstract class intended to be used in projects where the user controls various parameter settings. It can deal with any combination of parameter types (strings, integers, doubles, enumerations, booleans, ...). It contains a utility for dealing with "key=value" pairs, and importantly it has a built-in mechanism for generating a Swing dialog to display/edit parameters.
## How to use ParameterBlock
- For each collection of parameters in the application, create a class  that extends `ParameterBlock`.
 - In that class, declare public static integer fields indexing each distinct parameter (so that you can access parameters via symbolic  names).
 - Give the class a public constructor.
 -- Begin the constructor by calling `super()` with the number of parameters in the class.
-- Fill the arrays `name[]`, `type[]`, `defaultValue[]` and `tip[]` (declared in the parent class) with each parameter's string label, class, default value and tool tip, respectively.
 -- Implement the `values()` method to return the values of any enumerated parameters.
- Within the program, create an instance of the subclass using the static `ParameterBlock.newBlock()` factory method.
 - If the ability to edit some parameters is contingent on other factors (possibly but not necessarily other parameter choices), use the `setEditable()` method to control whether they are editable and the `areEditable()` method to tell whether they are currently editable.
 - Use the `setValue()` and `getValue()` methods to set or get individual parameter values.
- In a Swing application, use the static `ParameterBlock.showDialog()` method to display a dialog window allowing the user to see and modify parameter settings. If changes are made by the user, use the `update()` method to change the values in the parameter block.

`ParameterBlock` also provides a utility method `split()` that takes as its argument the index of a parameter in a block. It assumes the parameter is a string consisting of multiple "key=value" pairs (separated by any combinations of commas, semicolons, colons, spaces or tabs), splits those pairs, and returns a matrix with keys in the first column and values in the second.
## Demonstration
The `demo` package contains a small Swing program that demonstrates most of the features of `ParameterBlock`. It simulates a situation in which a user selects problem solver and sets a time limit (in the `Parameters > General parameters`menu) and then selects parameter values specific to the chosen solver (in the `Parameters > Specific parameters` menu). Each time parameters are changes, the new results are displayed in the main window.